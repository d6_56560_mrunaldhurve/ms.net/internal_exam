﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace MrunalDhurve_56560_D6
{
    internal class Employee
    {

        public int EmpNo { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public double Salary { get; set; }
        public double Commision { get; set; }
        public int DeptNO { get; set; }

        public Employee()
        {

        }

        public Employee(int empNo, string name, string designation, double salary, double commision, int deptNO)
        {
            EmpNo = empNo;
            Name = name;
            Designation = designation;
            Salary = salary;
            Commision = commision;
            DeptNO = deptNO;
        }
        public double Calcluate_Total_Salary(double salary, double commision)
        {
            double total = salary + commision;
            return total;
        }

        public List<Employee> GetEmployeesByDept(int DeptNo, ArrayList Employeees)
        {
            List<Employee> list = new List<Employee>();
            for (int i = 0; i < Employeees.Count; i++)
            {
                object refTo = Employeees[i];
                if (refTo is Employee)
                {
                    Employee emp = (Employee)refTo;
                    if (emp.DeptNO == DeptNo)
                    {
                        list.Add(emp);
                    }
                }
            }
            return list;
        }

        public override string ToString()
        {
            return "EmpNO = " + EmpNo + "\nName = " + Name + "\nDesignation = " + Designation + "\nSalary = " + Salary
                    + "\nCommision = " + Commision + "\nDeptNO = " + DeptNO;
        }
    }
}
