﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MrunalDhurve_56560_D6
{
    internal class Department
    {
        public int DeptNO { get; set; }
        public string Deptname { get; set; }
        public string Location { get; set; }

        public Department(int deptNO, string deptname, string location)
        {
            DeptNO = deptNO;
            Deptname = deptname;
            Location = location;
        }

        public override string ToString()
        {
            return "DeptNO = " + DeptNO + "\nDeptName = " + Deptname + "\nLocation = " + Location;
        }
    }
}
