﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace MrunalDhurve_56560_D6
{
    internal class Program
    {
        static void Main(string[] args)
        {

            ArrayList arrayList = new ArrayList();
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("0. Exit\n1. Add Employee\n2. Add Department\n3. Show all Employee" +
                                  "\n4. Show all Departments\n5. Show total salary of Particular Employee" +
                                  "\n6. Show total salary of all employees\n7. Display all employees of particular department " +
                                  "\n8. Display Department wise count of employees\n9. Display Department wise Average salary" +
                                  "\n10. Display Department wise Minimum salary");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 0:
                        Console.WriteLine("Logged out successfully");
                        flag = false;
                        break;

                    case 1:
                        Console.WriteLine("Enter EmpNo, Name, Designaion, Salary, Commision, DeptNo");
                        Employee emp = new Employee(Convert.ToInt32(Console.ReadLine()),
                                                         Console.ReadLine(),
                                                         Console.ReadLine(),
                                                         Convert.ToInt32(Console.ReadLine()),
                                                         Convert.ToInt32(Console.ReadLine()),
                                                         Convert.ToInt32(Console.ReadLine()));
                        arrayList.Add(emp);
                        Console.WriteLine("Employee added \n");
                        break;

                    case 2:
                        Console.WriteLine("Enter DeptNO, DeptName, Location");
                        Department dept = new Department(Convert.ToInt32(Console.ReadLine()),
                                                         Console.ReadLine(),
                                                         Console.ReadLine());
                        arrayList.Add(dept);
                        Console.WriteLine("Department added \n");
                        break;

                    case 3:
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            object refTO = arrayList[i];
                            if (refTO is Employee Show)
                            {
                                Console.WriteLine(Show.ToString());
                            }
                        }
                        break;

                    case 4:
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            object refTO = arrayList[i];
                            if (refTO is Department ShowDept)
                            {
                                Console.WriteLine(ShowDept.ToString());
                            }
                        }
                        break;

                    case 5:
                        Console.WriteLine("Enter EmpNo");
                        int empNO = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            object refTO = arrayList[i];
                            if (refTO is Employee)
                            {
                                Employee employee2 = (Employee)refTO;
                                if (employee2.EmpNo == empNO)
                                {
                                    double total_salary = employee2.Calcluate_Total_Salary(employee2.Salary, employee2.Commision);
                                    Console.WriteLine("Total salary of EmpNo " + empNO + " is " + total_salary);
                                }
                            }
                        }
                        break;

                    case 6:
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            object refTO = arrayList[i];
                            if (refTO is Employee)
                            {
                                Employee employee2 = (Employee)refTO;
                                double total_salary = employee2.Calcluate_Total_Salary(employee2.Salary, employee2.Commision);
                                Console.WriteLine("Total salary of EmpNo " + employee2.EmpNo + " is " + total_salary);
                            }
                        }
                        break;

                    case 7:
                        Console.WriteLine("Enter DeptNO");
                        int Dept = Convert.ToInt32(Console.ReadLine());
                        Employee employee3 = new Employee();
                        List<Employee> list = employee3.GetEmployeesByDept(Dept, arrayList);
                        for (int i = 0; i < list.Count; i++)
                        {
                            Employee employee4 = list[i];
                            Console.WriteLine(employee4.ToString());
                        }
                        break;

                    case 8:
                        List<Employee> list1 = new List<Employee>();
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            object refTo = arrayList[i];
                            if (refTo is Employee sortedEmployee)
                            {
                                list1.Add(sortedEmployee);
                            }
                        }
                        var EmploeesCount = list1.GroupBy(x => x.DeptNO)
                                  .Select(y => new { Department = y.Key, EmployeesCount = y.Count() })
                                  .ToList();
                        Console.WriteLine("Count of Employees");
                        for (int i = 0; i < EmploeesCount.Count; i++)
                        {
                            object refTo = EmploeesCount[i];
                            Console.WriteLine(refTo);
                        }
                        break;

                    case 9:
                        List<Employee> List2 = new List<Employee>();
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            object refTo = arrayList[i];
                            if (refTo is Employee sortedEmployee)
                            {
                                List2.Add(sortedEmployee);
                            }
                        }
                        var AvgSalaryByDepartment = List2.GroupBy(x => x.DeptNO)
                                  .Select(q => new { Department = q.Key, Average_Salary = q.Average(x => x.Salary) })
                                  .ToList();
                        Console.WriteLine("Average Salary of Employees");
                        for (int i = 0; i < AvgSalaryByDepartment.Count; i++)
                        {
                            object refTo = AvgSalaryByDepartment[i];
                            Console.WriteLine(refTo);
                        }
                        break;

                    case 10:
                        List<Employee> List3 = new List<Employee>();
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            object refTo = arrayList[i];
                            if (refTo is Employee sortedEmployee)
                            {
                                List3.Add(sortedEmployee);
                            }
                        }
                        var MInimumSalaryByDepartment = List3.GroupBy(x => x.DeptNO)
                                  .Select(a => new { Department = a.Key, Minimum_Salary = a.Min(x => x.Salary) })
                                  .ToList();
                        Console.WriteLine("Minimum Salary of Employees");
                        for (int i = 0; i < MInimumSalaryByDepartment.Count; i++)
                        {
                            object refTo = MInimumSalaryByDepartment[i];
                            Console.WriteLine(refTo);
                        }
                        break;

                    default:
                        Console.WriteLine("Invalid Choice\n");
                        break;
                }

            }
            Console.WriteLine();
                }
    }
}
